<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link type="text/css" href="list.css" rel="stylesheet" />
<title>Phones</title>
</head>
<body>
    <table border=1>
    	<caption>Phonebook App</caption>
        <thead>
            <tr>
                <th>Phone ID</th>
                <th>Number</th>
                <th>FIO</th>
                <th colspan=2>Action</th>
            </tr>
        </thead>
        <tbody>
            <c:forEach items="${phones}" var="phone">
                <tr>
                    <td><c:out value="${phone.phoneId}" /></td>
                    <td><c:out value="${phone.number}" /></td>
                    <td><c:out value="${phone.fio}" /></td>
                    <td><a href="PhoneController?action=edit&phoneid=<c:out value="${phone.phoneId}"/>">Update</a></td>
                    <td><a href="PhoneController?action=delete&phoneid=<c:out value="${phone.phoneId}"/>">Delete</a></td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
    <p><a href="PhoneController?action=insert">Add Phone</a></p>
    <p><a href="PhoneController?action=search">Search</a></p>
    <p><a href="PhoneController?action=listPhone">Clear search</a></p>
</body>
</html>