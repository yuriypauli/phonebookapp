<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Add/update phone</title>
</head>
<body>
    <script>
        $(function() {
            $('input[name=dob]').datepicker();
        });
    </script>

    <form method="POST" action='PhoneController' name="frmAddUser">
        Phone ID : <input type="text" readonly="readonly" name="phoneid"
            value="<c:out value="${phone.phoneId}" />" /> <br />
        Number : <input
            type="text" name="number"
            value="<c:out value="${phone.number}" />" /> <br />
        FIO : <input
            type="text" name="fio"
            value="<c:out value="${phone.fio}" />" /> <br /> <input
            type="submit" value="Submit" />
    </form>
</body>
</html>