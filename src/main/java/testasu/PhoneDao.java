package testasu;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class PhoneDao {

    public void addPhone(Phone phone) {
        Transaction trns = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            trns = session.beginTransaction();
            session.save(phone);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            if (trns != null) {
                trns.rollback();
            }
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
    }

    public void deletePhone(int phoneid) {
        Transaction trns = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            trns = session.beginTransaction();
            Phone phone = (Phone) session.load(Phone.class, new Integer(phoneid));
            session.delete(phone);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            if (trns != null) {
                trns.rollback();
            }
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
    }

    public void updatePhone(Phone phone) {
        Transaction trns = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            trns = session.beginTransaction();
            session.update(phone);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            if (trns != null) {
                trns.rollback();
            }
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
    }

    public List<Phone> getAllPhones() {
        List<Phone> phones = new ArrayList<Phone>();
        Transaction trns = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            trns = session.beginTransaction();
            phones = session.createQuery("from Phone").list();
        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return phones;
    }

    public List<Phone> getPhonesByParams(String number, String fio) {
        List<Phone> phones = new ArrayList<Phone>();
        Transaction trns = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            trns = session.beginTransaction();
            String queryString = "from Phone where number = :number and fio = :fio";
            Query query = session.createQuery(queryString);
            query.setString("number", number);
            query.setString("fio", fio);
            phones = query.list();
        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return phones;
    }

    public List<Phone> getPhonesByNumber(String number) {
        List<Phone> phones = new ArrayList<Phone>();
        Transaction trns = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            trns = session.beginTransaction();
            String queryString = "from Phone where number = :number";
            Query query = session.createQuery(queryString);
            query.setString("number", number);
            phones = query.list();
        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return phones;
    }

    public List<Phone> getPhonesByFio(String fio) {
        List<Phone> phones = new ArrayList<Phone>();
        Transaction trns = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            trns = session.beginTransaction();
            String queryString = "from Phone where fio = :fio";
            Query query = session.createQuery(queryString);
            query.setString("fio", fio);
            phones = query.list();
        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return phones;
    }

    public Phone getPhoneById(int phoneid) {
        Phone phone = null;
        Transaction trns = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            trns = session.beginTransaction();
            String queryString = "from Phone where id = :id";
            Query query = session.createQuery(queryString);
            query.setInteger("id", phoneid);
            phone = (Phone) query.uniqueResult();
        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return phone;
    }
}