package testasu;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class PhoneController extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private static String INSERT_OR_EDIT = "/phone.jsp";
    private static String LIST_USER = "/listPhone.jsp";
    private static String SEARCH = "/search.jsp";
    private PhoneDao dao;

    public PhoneController() {
        super();
        dao = new PhoneDao();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String forward="";
        String action = request.getParameter("action");

        if (action.equalsIgnoreCase("delete")){
            int phoneId = Integer.parseInt(request.getParameter("phoneid"));
            dao.deletePhone(phoneId);
            forward = LIST_USER;
            request.setAttribute("phones", dao.getAllPhones());
        } else if (action.equalsIgnoreCase("edit")){
            forward = INSERT_OR_EDIT;
            int phoneId = Integer.parseInt(request.getParameter("phoneid"));
            Phone phone = dao.getPhoneById(phoneId);
            request.setAttribute("phone", phone);
        } else if (action.equalsIgnoreCase("listPhone")){
            forward = LIST_USER;
            String number = request.getParameter("number");
            String fio = request.getParameter("fio");
            if ("".equals(number)) {
                number = null;
            }
            if ("".equals(fio)) {
                fio = null;
            }
            if (fio != null && number != null) {
                request.setAttribute("phones", dao.getPhonesByParams(number, fio));
            } else if (fio != null) {
                request.setAttribute("phones", dao.getPhonesByFio(fio));
            } else if (number != null) {
                request.setAttribute("phones", dao.getPhonesByNumber(number));
            } else {
                request.setAttribute("phones", dao.getAllPhones());
            }
        }  else if (action.equalsIgnoreCase("search")){
        forward = SEARCH;
    }
        else {
            forward = INSERT_OR_EDIT;
        }

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Phone phone = new Phone();
        phone.setNumber(request.getParameter("number"));
        phone.setFio(request.getParameter("fio"));
        String phoneid = request.getParameter("phoneid");
        if(phoneid == null || phoneid.isEmpty())
        {
            dao.addPhone(phone);
        }
        else
        {
            phone.setPhoneId(Integer.parseInt(phoneid));
            dao.updatePhone(phone);
        }
        RequestDispatcher view = request.getRequestDispatcher(LIST_USER);
        request.setAttribute("phones", dao.getAllPhones());
        view.forward(request, response);
    }
}
