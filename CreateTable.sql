USE UserDB;
CREATE TABLE `phones` (
  `phoneid` int(11) NOT NULL,
  `number` varchar(100) DEFAULT NULL,
  `fio` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`phoneid`),
  UNIQUE KEY `id_UNIQUE` (`phoneid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;